<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <!-- Basic Blade Template Created Using: LaraBit -->
    <div class="container">
        <h1>Sanber Book</h1><br>
        <h3>Sosial Media Developer santai berkualitas</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid id quas voluptas nulla dignissimos magnam reprehenderit consequuntur sapiente perspiciatis totam pariatur adipisci officiis eius ratione, exercitationem cupiditate, quis ipsum magni!</p>
        <h3>Benefit Join Sanber Book</h3>
        <ul>
            <li>Mentor Berkualitas</li>
            <li>Teman-teman yang solid</li>
            <li>Relasi yang kuat</li>

        </ul>
        <h3>Cara Bergabung ke Sanber</h3>
        <ol>
            <li>Isi Registrasi</li>
            <li>Ikuti Seleksi</li>
            <li>Pengumuman</li>
            <li>Brefing Bersama</li>
        </ol>

        <a href="/register" class="btn btn-success">Klik Disini</a>    
    </div>
    
</body>

</html>
