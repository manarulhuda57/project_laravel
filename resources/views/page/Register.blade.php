<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <!-- Basic Blade Template Created Using: LaraBit -->
    <div class="container">
        <h1>Buat Akun Baru</h1>

        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label>Nama Depan</label><br>
            <input type="text" name="fname"> <br><br>
            <label>Nama Belakang</label> <br>
            <input type="text" name="lname"> <br><br>
            <label>Gender</label> <br>
            <input type="radio" name="gender" value="men">
            <label>Men</label> <br>
            <input type="radio" name="gender" value="women">
            <label>Women</label> <br>
            <input type="radio" name="gender" value="other">
            <label>Other</label> <br><br>

            <label>Nationaly : </label>
            <select name="nationaly" id="nationaly">
                <option value="ind">Indonesia</option>
                <option value="mls">Malaysia</option>
                <option value="sin">Singapura</option>
                <option value="bd">Brunei Darussalam</option>
            </select><br><br>

            <label>Laguage Spoken : </label><br>
            <input type="checkbox" name="b_indo" id="b_indo">
            <label>Bahasa Indonesia</label><br>
            <input type="checkbox" name="b_mela" id="b_mela">
            <label>Bahasa Melayu</label><br>
            <input type="checkbox" name="b_ingg" id="b_ingg">
            <label>Bahasa Inggris</label><br><br>
            
            <label>Bio</label><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" class="btn btn-success" value="Sign Up"><br><br>
        </form>
    </div>
    
</body>

</html>
