<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function register(){
        return view('page/Register');
    }

    public function welcome(Request $request){
        // dd($request->all());
        $depan = $request['fname'];
        $belakang = $request['lname'];

        return view('page.welcome', compact('depan', 'belakang'));
    }

}
