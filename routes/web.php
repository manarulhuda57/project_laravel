<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

use App\Http\Controllers\FormController;
use App\Http\Controllers\HomeController;

Route::get('/', 'HomeController@Home' );
Route::get('/register', 'FormController@register' );
Route::post('/welcome', 'FormController@welcome' );


